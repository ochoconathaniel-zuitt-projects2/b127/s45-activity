import {useState, useEffect, Fragment} from 'react'
import {Button} from 'react-bootstrap'

export default function Counter(){
	
	const [count, setCount] = useState(0)

	useEffect(() =>{

		document.title = `You clicked ${count} times`

	}, [count])

	return(

			<Fragment>
				<p class="mt-5">You clicked {count}</p>
				<Button variant="primary" onClick={() => setCount(count + 1)} > Click me </Button>
			</Fragment>	

		)
}