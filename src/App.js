import { Fragment, useState } from 'react';
import './App.css';
import AppNavbar from './components/AppNavbar';
//pages
import Home from './pages/Home';
import Courses from './pages/Courses'
import Counter from './components/Counter'
import Register from './pages/Register'
import Login from './pages/Login'
import { BrowserRouter as Router } from 'react-router-dom';
import {Route,Switch} from 'react-router-dom';
import Error from './pages/Error'
//Bootstrap
import { Container } from 'react-bootstrap';

import {UserProvider} from './UserContext';

function App() {
 
  const[user, setUser] = useState({email: localStorage.getItem('email')})






  return (
   <UserProvider value={ {user,setUser} }>
    <Router>
      < AppNavbar />
      <Container>
       <Switch>
        
        
        < Route exact path="/home" component={Home} />
        < Route exact path="/courses" component={Courses} />
        < Route exact path="/register" component={Register} />
        < Route exact path="/login" component={Login} />
        < Route component={Error} />
      </Switch>
      </Container>
    </Router>
  </UserProvider>
  );
}

export default App;
