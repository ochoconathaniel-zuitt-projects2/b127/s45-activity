import {Fragment, useState, useEffect} from 'react';
import { Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2'

export default function Register() {
	
		const [email,setEmail] = useState('');
		const [password1,setPassword1] = useState('');
		const [password2,setPassword2] = useState('');

		const [isActive, setIsActive] = useState(false);


		useEffect(()=> {
			if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){

				setIsActive(true);

			} else {
				setIsActive(false);
			}




		},[email, password1, password2])

	function registerUser(e){
		e.preventDefault();

			setEmail('')
			setPassword1('')
			setPassword2('')


		Swal.fire({

			title: 'Yaaaaaaay!!!',
			icon: 'success',
			text: 'salamat kaibigan'

		})
	}


		return(
		<Fragment>
		<h1 class="mt-4">Register</h1>
		<Form onSubmit ={(e) => registerUser(e)}>
			<Form.Group>
				<Form.Label>Email address:</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required

				/>
				<Form.Text className="text-muted">
					We'll never share your email with anyone else.
				</Form.Text>
				</Form.Group>
				<Form.Group>
					<Form.Label>Password</Form.Label>
					<Form.Control
						type="password"
						placeholder="Verify your Password"
						value={password1}
						onChange={e => setPassword1(e.target.value)}
						required

						/>	
					</Form.Group>
					<Form.Group>
						<Form.Label>Verify Password
						</Form.Label>
						<Form.Control
							type="password"
							placeholder="Verify your Password"
							value={password2}
							onChange={e => setPassword2(e.target.value)}
							required
							/>

						</Form.Group>
						{isActive ?
						<Button variant="primary" type="submit" id="submitBtn">Submit</Button>
						:
						<Button variant="primary" type="submit" id="submitBtn" disabled>Submit</Button>
						}
					</Form>
					</Fragment>







			)


	}